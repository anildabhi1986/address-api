/*
 * @description: use for shipping address
 * @return: address1, address2, city, zipcode
 */
function spi_shipping() {
    new daum.Postcode({
        oncomplete: function(data) {
            var fullAddr = '';
            var extraAddr = '';

            if (data.userSelectedType === 'R') {
                fullAddr = data.roadAddress;

            } else {
                fullAddr = data.jibunAddress;
            }

            if(data.userSelectedType === 'R'){
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            $('#shippng-postcode1').val(data.postcode1);
            $('#shippng-postcode2').val(data.postcode2);
            $('.row-shipping-address1').find("input[name$='_address1']").val(fullAddr).prev('.input-block-hint').hide();
            $('.row-shipping-city').find("input[name$='_city']").val(data.sido);
            $('.row-shipping-zipcode').find("input[name$='_zip']").val(data.postcodeSeq);

            $('.row-shipping-address2').find("input[name$='_address2']").focus();
        }
    }).open();
}
/*
 * @description: use for billing address
 * @return: address1, address2, city, zipcode
 */
function spi_billing() {
    new daum.Postcode({
        oncomplete: function(data) {
            var fullAddr = '';
            var extraAddr = '';

            if (data.userSelectedType === 'R') {
                fullAddr = data.roadAddress;

            } else {
                fullAddr = data.jibunAddress;
            }

            if(data.userSelectedType === 'R'){
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            $('#billing-postcode1').val(data.postcode1);
            $('#billing-postcode2').val(data.postcode2);
            $('.row-billing-address1').removeClass('invalid').find("input[name$='_address1']").val(fullAddr).prev('.input-block-hint').hide();
            $('.row-billing-city').find("input[name$='_city']").val(data.sido);
            $('.row-billing-zipcode').find("input[name$='_zip']").val(data.postcodeSeq);

            $('.row-billing-address2').find("input[name$='_address2']").focus();
        }
    }).open();
}
/*
 * @description: use for customer address
 * @return: address1, address2, city, zipcode
 */
function spi_address() {
    new daum.Postcode({
        oncomplete: function(data) {
            var fullAddr = '';
            var extraAddr = '';

            if (data.userSelectedType === 'R') {
                fullAddr = data.roadAddress;

            } else {
                fullAddr = data.jibunAddress;
            }

            if(data.userSelectedType === 'R'){
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            $('#address-postcode1').val(data.postcode1);
            $('#address-postcode2').val(data.postcode2);
            $('.row-address-address1').removeClass('invalid').find("input[name$='_address1']").val(fullAddr).prev('.input-block-hint').hide();
            $('.row-address-city').find("input[name$='_city']").val(data.sido);
            $('.row-address-zip').find("input[name$='_zip']").val(data.postcodeSeq);

            $('.row-address-address2').find("input[name$='_address2']").focus();
            $('.luana-module-account-edit').find("input[name$='_edit']").removeAttr("disabled");
        }
    }).open();
}